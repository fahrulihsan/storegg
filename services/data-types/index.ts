interface categoryType {
    _id: string;
    name: string;
    __v: string;
}

export interface GameItemType {
    _id: string;
    status: string;
    name: string;
    thumbnail: string;
    category: categoryType;
}

interface BankType {
    _id: string;
    name: string;
    bankName: string;
    noRekening: string;
}
export interface paymentType {
    _id: string;
    name: string;
    type: string;
    status: string;
    banks: BankType[]
}
export interface nominalType {
    _id: string;
    coinQuantity: number;
    coinName: string;
    price: number;
  
}