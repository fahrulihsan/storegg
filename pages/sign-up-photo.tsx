import React, { useCallback, useEffect, useState } from 'react';
import { getGameCategory } from '../services/player';
import Image  from 'next/image';
import { setSignUp } from '../services/auth';

export default function SignUpPhoto() {
  const [categories, setCategories] = useState([])
  const [favorite, setFavorite] = useState("")
  const [image, setImage] = useState("")
  const [imagePreview, setImagePreview] = useState("/icon/upload.svg")
  const [LocalForm, setLocalForm] = useState({
    email: "",
    name: "",
    password: ""
  })

  const getGameCategoryApi = useCallback(async() => {
    const data = await getGameCategory()
    setCategories(data)
    setFavorite(data[0].id)
  },[getGameCategory])
  
  
  useEffect(() => {
    getGameCategoryApi()
  },[])

  useEffect(() => {
    const formData = localStorage.getItem("user-form");
    console.log("JSON.parse(formData)", JSON.parse(formData))
    setLocalForm(JSON.parse(formData))
  },[])
  
  const onSubmit = async () => {
    const data = new FormData();

    data.append("image", image);
    data.append("email", LocalForm.email)
    data.append("name", LocalForm.name )
    data.append("password", LocalForm.password)
    data.append("username", LocalForm.name)
    data.append("phoneNumber", "089213131232")
    data.append("role", "user")
    data.append("status", "Y")
    data.append("favorite", favorite)
    
    try {
      let response = await setSignUp(data)
    } catch (error) {
      console.log(error)
    }

  }

  return (
    <section className="sign-up-photo mx-auto pt-lg-227 pb-lg-227 pt-130 pb-50">
      <div className="container mx-auto">
        <form action="">
          <div className="form-input d-md-block d-flex flex-column">
            <div>
              <div className="mb-20">
                <div className="image-upload text-center">
                  <label htmlFor="avatar">
                    <Image className='image-upload' src={imagePreview} width={120} height={120} alt="upload" />
                  </label>
                  <input 
                    id="avatar" 
                    type="file" 
                    name="avatar" 
                    accept="image/png, image/jpeg" 
                    onChange={(e) => {
                      const img = e.target.files[0]
                      setImage(img)
                      setImagePreview(URL.createObjectURL(img))
                    }} 
                  />
                </div>
              </div>
              <h2 className="fw-bold text-xl text-center color-palette-1 m-0">{LocalForm.name ? LocalForm.name : "" }</h2>
              <p className="text-lg text-center color-palette-1 m-0">{LocalForm.email ? LocalForm.email : ""}</p>
              <div className="pt-50 pb-50">
                <label htmlFor="category" className="form-label text-lg fw-medium color-palette-1 mb-10">
                  Favorite
                  Game
                </label>
                <select
                  id="category"
                  name="category"
                  className="form-select d-block w-100 rounded-pill text-lg"
                  aria-label="Favorite Game"
                  onChange={(e) => setFavorite(e.target.value)}
                >
                  {
                    categories.map(category => {
                      return (
                        <option value={category._id} selected>{category.name}</option>
                      )
                    })
                  }
                </select>
              </div>
            </div>

            <div className="button-group d-flex flex-column mx-auto">
              <button
                className="btn btn-create fw-medium text-lg text-white rounded-pill mb-16"
                type="button"
                onClick={onSubmit}
              >
                Create My Account
              </button>
              <a
                className="btn btn-tnc text-lg color-palette-1 text-decoration-underline pt-15"
                href="/#"
                role="button"
              >
                Terms &
                Conditions
              </a>
            </div>
          </div>
        </form>
      </div>
    </section>
  );
}
